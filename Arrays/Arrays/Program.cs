﻿using System;
using System.Collections.Generic;

namespace Arrays
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            List<string> generalFoods = new List<string>()
            {
                "tacos",
                "beans",
                "milk",
                "bread",
                "lettuce",
                "hummus",
                "pizza",
                "apple",
                "burger",
                "apple burger",
                "pine burger"
            };

            List<string> burgers = new List<string>();
            foreach (var item in generalFoods)
            {
                if (item.Contains("burger"))
                {
                    burgers.Add(item);
                }
            }

            foreach (var item in burgers)
            {
                Console.WriteLine(item);
            }
            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }
    }
}