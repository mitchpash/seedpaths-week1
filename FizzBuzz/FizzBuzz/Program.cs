﻿using System;

namespace FizzBuzz
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            for (int i = 0; i <= 20; i++)
            {
                Console.WriteLine(FizzBuzz(i));
            }
            //End of Program
            Console.WriteLine();
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static string FizzBuzz(int number)
        {
            if (number % 5 == 0)
            {
                if (number % 5 == 0 && number % 3 == 0)
                {
                    return "FizzBuzz";
                }
                return "buzz";
            }
            if (number % 3 == 0)
            {
                return "fizz";
            }
            return number.ToString();
        }
    }
}