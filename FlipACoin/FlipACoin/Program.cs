﻿using System;

namespace FlipACoin
{
    internal class Program
    {
        public static Random random = new Random();
        public static int heads = 0;
        public static int tails = 0;

        private static void Main(string[] args) 
        {
            int speed = 1000;
            int flips = 7;

            for (int i = 0; i < flips; i++)
            {
                Console.WriteLine("Flipping...");
                System.Threading.Thread.Sleep(speed);

                FlipForHeads();
                System.Threading.Thread.Sleep(speed / 3);
            }
            Console.WriteLine("Your coin landed on heads {0} times", heads);
            Console.WriteLine("Your coin landed on tails {0} times", tails);
            Console.WriteLine();

            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static void FlipForHeads()
        {
            int count = 1;
            while (FlipACoin() != 1)
            {
                count++;
                tails++;
                //Console.WriteLine("Count is: {0}", count);
            }
            heads++;
            Console.WriteLine("It took {0} time(s) to get heads", count);
            Console.WriteLine();
        }

        public static void FlipForTails()
        {
            int count = 1;
            while (FlipACoin() != 0)
            {
                count++;
                heads++;
                //Console.WriteLine("Count is: {0}", count);
            }
            tails++;
            Console.WriteLine("It took {0} time(s) to get tails", count);
            Console.WriteLine();
        }

        public static int FlipACoin()
        {
            return random.Next(2);
        }
    }
}