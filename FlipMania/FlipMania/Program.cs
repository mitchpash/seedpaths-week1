﻿using System;

namespace FlipMania
{
    internal class Program
    {
        public static Random random = new Random();

        
        private static void Main(string[] args)
        {
            FlipCoins(10000);
            FlipForHeads(10000);

            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        //Create a function called FlipForHeads which takes an integer called “numberOfHeads” and does the following operations:
        //i.Declare counters for the numberOfHeadsFlipped, and totalFlips.Also declare a new instance of the Random object for flipping.
        //ii.Create a while loop that loops until the numberOfHeadsFlipped equals the numberOfHeads argument.
        //iii.Inside of your loop, flip a coin using the Random object’s.Next() method.
        //iv.Add 1 to the numberOfFlips counter.
        //v.If the flip was a head, increment the numberOfHeadsFlipped counter.
        //vii.After the loop completes, write the following output to the console: “We are flipping a coin until we find <numberOfHeads> heads” “It took <totalFlips> to find <numberOfHeads> heads”
        public static void FlipForHeads(int numberOfHeads)
        {
            int numberOfHeadsFlipped = 0;
            int numberOfFlips = 0;

            while (numberOfHeadsFlipped != numberOfHeads)
            {
                if(random.Next(2) == 0)
                {
                    numberOfHeadsFlipped++;
                }
                numberOfFlips++;
            }

            Console.WriteLine("We are flipping a coin until we find {0} heads", numberOfHeads);
            Console.WriteLine("It took {0} flips to find {1} heads", numberOfFlips, numberOfHeads);
        }

        //Create a function called FlipCoins that takes an integer called “numberOfFlips” and does the follow operations:
        //i.Declare counters for the numberOfHeads, and the numberOfTails
        //ii.Declare a new instance of the Random object (for flipping)
        //iii.Create a loop that will repeat as many times as indicated by the “numberOfFlips” arguement
        //iv.Inside the loop, flip a coin using the Random object’s.Next() function
        //v.Determine if the flip was a heads (0), or a tails(1) and increment the respective counter by 1.
        //vi.After your loop is complete, write the following output to the console: “We flipped a coin<numberOfFlips> times.” “Number of Heads: <numberOfHeads>” "Number of Tails: <numberOfTails>”
        public static void FlipCoins(int numberOfFlips)
        {
            int numberOfHeads = 0;
            int numberOfTails = 0;

            for (int i = 0; i < numberOfFlips; i++)
            {
                if (random.Next(2) == 0)
                {
                    numberOfHeads++;
                }
                else
                {
                    numberOfTails++;
                }
            }
            Console.WriteLine("We flipped a coin {0} times.", numberOfFlips);
            Console.WriteLine("Number of Heads: {0}", numberOfHeads);
            Console.WriteLine("Number of Tails: {0}", numberOfTails);
        }
    }
}