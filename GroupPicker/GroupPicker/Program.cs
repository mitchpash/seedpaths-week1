﻿using System;
using System.Collections.Generic;

namespace GroupPicker
{
    internal class Program
    {
        public static Random rand = new Random();

        public static List<string> studentList = new List<string>() { "Mahmoud", "Matt", "Kris", "Keith", "David", "Umar", "Nathan", "Ryan", "Colton", "Mac", "Aaron", "Mitch", "Lamond" };

        private static void Main(string[] args)
        {
            PickGroups(studentList, 4);
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static void PickGroups(List<string> studentList, int groupSize)
        {
            List<string> currentGroupList = new List<string>();
            int groupNumber = 1;

            while (studentList.Count != 0)
            {
                string currentStudent = studentList[rand.Next(studentList.Count)];
                currentGroupList.Add(currentStudent);
                //remove current student from studentList
                studentList.Remove(currentStudent);

                if (currentGroupList.Count.Equals(groupSize) || studentList.Count == 0)
                {
                    Console.WriteLine("Group {0}", groupNumber);
                    Console.WriteLine("_________");
                    foreach(string student in currentGroupList)
                    {
                        Console.WriteLine(student);
                    }
                    currentGroupList.Clear();
                    groupNumber++;
                    Console.WriteLine();
                }
            }
        }
    }
}