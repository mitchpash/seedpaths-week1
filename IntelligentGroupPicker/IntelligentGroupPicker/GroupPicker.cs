﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteligentGroupPicker
{
    class GroupPicker
    {
        private Random rng = new Random();
        public List<List<string>> StudentsByNGroups { get; private set; } = new List<List<string>>();

        /// <summary>
        /// Builds a group of students sorted by a number of students per group.
        /// </summary>
        /// <param name="studentList"></param>
        /// <param name="studentsPerGroup"></param>
        public GroupPicker(List<string> studentList, int studentsPerGroup)
        {
            StudentsByNGroups = PickGroups(studentList, studentsPerGroup);
        }

        private List<List<string>> PickGroups(List<string> studentList, int numberOfGroups)
        {
            List<List<string>> populatedStudentsMatrix = new List<List<string>>();
            //While there is a student in the list
            while (studentList.Count > 0)
            {
                List<string> currentStudentList = new List<string>();

                for (int i = 0; i < numberOfGroups && studentList.Count > 0; i++)
                {
                    var randomStudent = studentList[rng.Next(studentList.Count)];
                    currentStudentList.Add(randomStudent);
                    studentList.Remove(randomStudent);
                }
                populatedStudentsMatrix.Add(currentStudentList);
            }

            checkForStragglingGroups(populatedStudentsMatrix, numberOfGroups);

            return populatedStudentsMatrix;
        }

        private void checkForStragglingGroups(List<List<string>> studentsByNGroups, int numberOfGroups)
        {
            foreach (var studentList in studentsByNGroups)
            {
                if (studentList.Count < numberOfGroups)
                {
                    Console.WriteLine("WARNING: There are stragglers in your list.");
                }
            }
        }
    }
}
