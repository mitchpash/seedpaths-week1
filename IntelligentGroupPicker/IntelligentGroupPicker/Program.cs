﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteligentGroupPicker
{
    class Program
    {
        public static List<string> studentList = new List<string>() { "Mitch", "Mahmoud", "Keith", "Lamond", "Ryan", "Kris", "Matt", "Aaron", "David", "Colton", "Mac", "Nathan", "Umar" };
        static void Main(string[] args)
        {
            GroupPicker groupPicker = new GroupPicker(studentList, 4);

            int counter = 1;

            foreach (var sublist in groupPicker.StudentsByNGroups)
            {
                Console.WriteLine("|Group {0}|", counter);
                foreach (var item in sublist)
                {
                    Console.Write("| {0} ", item);
                }
                counter++;
                Console.WriteLine("|");
            }

            Console.ReadKey();
        }
    }
}
