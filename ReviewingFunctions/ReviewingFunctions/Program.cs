﻿using System;

namespace ReviewingFunctions
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            NumberOfLettersIn("l", "Mitchell");
            NumberOfLettersIn("S", "Sally is sunny");

            Console.WriteLine(AnnoyingTextGenerator("I'm sorry dave, I'm afraid I can't do that..."));

            Console.WriteLine(RoundNumber(34));

            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }
        /// <summary>
        /// rounds an integer to the nearest 5
        /// 1 % 5 = 1 down
        /// 2 % 5 = 2 down
        /// 3 % 5 = 3 up
        /// 4 % 5 = 4 up
        /// 5 % 5 = 0 nothing
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static int RoundNumber(int number)
        {
            int nearestRoundNumber = number;
            //33
            if (number%5 < 3)
            {
                nearestRoundNumber -= (nearestRoundNumber % 5);
            }
            else
            {
                nearestRoundNumber += (5 - (nearestRoundNumber % 5));
            }

            return nearestRoundNumber;
        }


        /// <summary>
        /// Returns an annoying string of text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string AnnoyingTextGenerator(string text)
        {
            string result = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                string letter = text[i].ToString();
                if (i % 2 == 0)
                {
                    result += letter.ToUpper();
                }
                else
                {
                    result += letter.ToLower();
                }                
            }
            return result;
        }

        /// <summary>
        /// Create a counter for how many times <letter> appears in <search>
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="search"></param>
        private static void NumberOfLettersIn(string letter, string search)
        {
            int matches = 0;
            //Data standardization
            letter = letter.ToLower();
            search = search.ToLower();

            for (int i = 0; i < search.Length; i++)
            {
                if(search[i] == letter[0])
                {
                    matches++;
                }
            }
            Console.WriteLine("The letter '{0}' appears in '{1}', {2} times.", letter, search, matches);
        }

    }
}