﻿using System;

namespace SwitchExample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("What color shirt are you wearing?: ");
            string shirtColor = Console.ReadLine();

            switch (shirtColor)
            {
                case "red":
                    Console.WriteLine("You're probably going to die");
                    break;

                case "black":
                case "purple":
                    Console.WriteLine("Go Rockies");
                    break;

                default:
                    break;
            }

            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }
    }
}