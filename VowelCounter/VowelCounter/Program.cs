﻿using System;

namespace VowelCounter
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Repeat counter until it is no longer needed
            bool continues = true;

            GetVowels();

            while (continues)
            {
                Console.Write("Would you like to find the vowels in another string of text? (Y/n): ");
                string yesOrNo = Console.ReadLine().ToLower();

                if (yesOrNo.Contains("yes") || yesOrNo.Contains("y"))
                {
                    GetVowels();
                }
                else if (yesOrNo.Contains("no") || yesOrNo.Contains("n"))
                {
                    Console.WriteLine("Thanks for using the magical vowel counter!");
                    continues = false;
                }
                else
                {
                    Console.WriteLine("Input not identified!");
                }
            }

            //End of program
            Console.Write("Press any key to contine...");
            Console.ReadKey();
        }

        private static void GetVowels()
        {
            string input;
            Console.WriteLine("Welcome to the magical vowel counter!");
            Console.Write("What text would you like to count?: ");
            input = Console.ReadLine();

            //Initialize
            Console.WriteLine("The number of vowels is: {0}", CountVowel(input));
        }

        public static int CountVowel(string text)
        {
            //Number of vowels returned
            int result = 0;

            //Array of vowels
            String[] values = { "a", "e", "i", "o", "u" };

            //Itterate through to find occurances of $values in $text
            foreach (var value in values)
            {
                if (text.Contains(value))
                {
                    result++;
                }
            }
            return result;
        }
    }
}